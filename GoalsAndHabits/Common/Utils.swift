import UIKit
import CloudKit


extension Calendar {
    func dayDates(from sDate: Date, to eDate: Date) -> [Date] {
        var dates = [Date]()
        for i in 0...dateComponents([.day], from: sDate, to: eDate).day! {
            let dateComps = dateComponents([.day, .month, .year], from: self.date(byAdding: .day, value: i, to: sDate)!)
            dates.append(date(from: dateComps)!)
        }
        return dates
    }
    
    func monthID(from date: Date) -> Double {
        let monthNum = component(.month, from: date)
        let monthNumberAsStr = String(monthNum).count == 1 ? "0".appending(String(monthNum)) : String(monthNum)
        let yearNumberAsStr = String(component(.year, from: date))
        return Double(yearNumberAsStr+monthNumberAsStr)!
    }
    
    func formatedMonthID(from strMonthID: String) -> String {
        let monthName = Calendar.current.monthSymbols[Int(strMonthID.suffix(2))!-1]
        return "\(monthName), \(strMonthID.prefix(4))"
    }
    
    
}

extension Date {
    var shortDayName: String {
        let f = DateFormatter()
        f.timeStyle = .none
        f.dateStyle = .full
        let name = f.string(from: self).components(separatedBy: ",")[0]
        return String(name[...name.index(name.startIndex, offsetBy: 2)])
    }

    
}

struct UtilServices {
    static let colorMap = ["Break Habit": #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), "Achieve Goal": #colorLiteral(red: 0.5807225108, green: 0.066734083, blue: 0, alpha: 1), "Monitor Activity": #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1), "blue": #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), "default": #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), "red": #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)]
}


