import UIKit


class AddOrEditGoalVC: MSViewCtrl<Displayable, AddOrEditGoalMC>, UITextFieldDelegate, UIPickerViewDelegate {
    @IBOutlet var popupView: UIView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var userEntriesSV: UIStackView!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var typeTF: UITextField!
    @IBOutlet var sDateTF: UITextField!
    @IBOutlet var eDateTF: UITextField!
    @IBOutlet var impTF: UITextField!
    @IBOutlet var visTF: UITextField!
    
    private let _dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .medium
        df.timeStyle = .none
        return df
    }()
    private var _datePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = .date
        dp.addTarget(self, action: #selector(dateSelectionChanged), for: .valueChanged)
        dp.backgroundColor = .white
        return dp
    }()
    private lazy var _typePV = _makePV(withTag: 0)
    private lazy var _impPV = _makePV(withTag: 1)
    private lazy var _visPV = _makePV(withTag: 2)
    
    var goalToEdit: Goal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mc = AddOrEditGoalMC(pc: Storage.shared.pc)
        _assignInputViews()
        if let goal = goalToEdit {
            _populate(with: goal)
        }
    }
    
    override func initMC() -> AddOrEditGoalMC {
        return AddOrEditGoalMC(pc: Storage.shared.pc)
    }
    
    private func _assignInputViews() {
        typeTF.inputView = _typePV
        sDateTF.inputView = _datePicker
        eDateTF.inputView = _datePicker
        impTF.inputView = _impPV
        visTF.inputView = _visPV
    }
    
    private func _populate(with goal: Goal) {
        titleLbl.text = "Edit Goal"
        nameTF.text = goal.name!
        typeTF.text = goal.type!.name!
        _typePV.selectRow(mc.goalTypes.firstIndex(of: goal.type!)!, inComponent: 0, animated: false)
        sDateTF.text = _dateFormatter.string(from: goal.sDate!)
        eDateTF.text = _dateFormatter.string(from: goal.eDate!)
        impTF.text = goal.importance!.name
        _impPV.selectRow(mc.goalImps.firstIndex(of: goal.importance!)!, inComponent: 0, animated: false)
        visTF.text = goal.isHidden ? GoalVisibility.hidden.rawValue : GoalVisibility.visible.rawValue
        _visPV.selectRow(goal.isHidden ? 1 : 0, inComponent: 0, animated: false)
    }
    
    private func validUserInput() -> Bool {
        for stackView in userEntriesSV.subviews {
            for controll in stackView.subviews {
                if controll is UITextField, (controll as! UITextField).text!.isEmpty {
                    return false
                }
            }
        }
        return true
    }
    
    private func _makePV(withTag tag: Int) -> UIPickerView {
        let pv = UIPickerView()
        pv.tag = tag
        pv.delegate = self
        pv.dataSource = mc
        pv.backgroundColor = .white
        return pv
    }
    
    @objc func dateSelectionChanged() {
        if sDateTF.isFirstResponder {
            sDateTF.text = _dateFormatter.string(from: _datePicker.date)
        } else {
            eDateTF.text = _dateFormatter.string(from: _datePicker.date)
        }
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        if !validUserInput() {
            return
        }
        let info = GoalInfo(name: nameTF.text!, typeIndex: _typePV.selectedRow(inComponent: 0), sDate: _dateFormatter.date(from: sDateTF.text!), eDate: _dateFormatter.date(from: eDateTF.text!), impIndex: _impPV.selectedRow(inComponent: 0), visIndex: _visPV.selectedRow(inComponent: 0))
        
        if let goal = goalToEdit {
            mc.update(goal, with: info)
        } else {
            mc.createGoal(withInfo: info)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.isEmpty {
            if typeTF.isFirstResponder {
                pickerView(_typePV, didSelectRow: 0, inComponent: 0)
                
            } else if impTF.isFirstResponder {
                pickerView(_impPV, didSelectRow: 0, inComponent: 0)
                
            } else if visTF.isFirstResponder {
                pickerView(_visPV, didSelectRow: 0, inComponent: 0)
            
            } else {
                textField.text = _dateFormatter.string(from: _datePicker.date)
            }
            
        } else {
            if typeTF.isFirstResponder, let g = goalToEdit {
                _typePV.selectRow(mc.goalTypes.firstIndex(of: g.type!)!, inComponent: 0, animated: true)
                
            } else if impTF.isFirstResponder, let g = goalToEdit {
                _impPV.selectRow(mc.goalImps.firstIndex(of: g.importance!)!, inComponent: 0, animated: true)
                
            } else if visTF.isFirstResponder, let g = goalToEdit {
                _visPV.selectRow(g.isHidden ? 1 : 0, inComponent: 0, animated: true)
            
            } else if !typeTF.isFirstResponder {
                let dateStr = sDateTF.isFirstResponder ? sDateTF.text! : eDateTF.text!
                _datePicker.date = _dateFormatter.date(from: dateStr)!
            }
            
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return mc.goalTypes[row].name!
        case 1:
            return mc.goalImps[row].name!
        case 2:
            return GoalVisibility.allCases[row].rawValue
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 0:
            typeTF.text = mc.goalTypes[row].name!
        case 1:
            impTF.text = mc.goalImps[row].name!
        case 2:
            visTF.text = GoalVisibility.allCases[row].rawValue
        default:
            break
        }
    }

    
}

enum GoalVisibility: String, CaseIterable {
    case visible = "Visible"
    case hidden = "Hidden"
}
