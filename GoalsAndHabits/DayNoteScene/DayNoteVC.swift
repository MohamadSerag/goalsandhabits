import UIKit

class DayNoteVC: UIViewController {
    @IBOutlet var noteTV: UITextView!
    var goalDay: GoalDay!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = String(Calendar.current.dateComponents([.day], from: goalDay.date!).day!)
        noteTV.text = goalDay.note
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if goalDay.note != noteTV.text {
            goalDay.note = noteTV.text
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !noteTV.isFirstResponder {
            noteTV.becomeFirstResponder()
        }        
    }
    
}

