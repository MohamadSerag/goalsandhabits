import CoreData
import UIKit


class GoalsMC: FrcTVDataSourceMC<Goal> {
    
    func fetchAllGoals() {
        frc.fetchRequest.predicate = NSPredicate(value: true)
        try! frc.performFetch()
    }
}
