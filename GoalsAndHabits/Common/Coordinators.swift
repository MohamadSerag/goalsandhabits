import UIKit


class AppCoordinator: NSObject {
    var splitVC: MSSplitVC
    var detailsNC: UINavigationController
    
    init(splitVC: MSSplitVC) {
        detailsNC = UIStoryboard(name: "GoalDays", bundle: Bundle.main).instantiateViewController(withIdentifier: "GDNC") as! UINavigationController
        self.splitVC = splitVC
        super.init()
        
        self.splitVC.delegate = self
        (detailsNC.topViewController as! GoalDaysVC).coordinator = self

        let masterNC = UIStoryboard(name: "Goals", bundle: Bundle.main).instantiateViewController(withIdentifier: "GoalsNC") as! UINavigationController
        (masterNC.topViewController as! GoalsVC).coordinator = self
        self.splitVC.viewControllers = [masterNC, detailsNC]
    }
    
    func addNewGoal() {
        let vc = UIStoryboard(name: "AddOrEditGoal", bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: AddOrEditGoalVC.self)) as! AddOrEditGoalVC
        vc.coordinator = self
        splitVC.present(vc, animated: true, completion: nil)
    }
    
    func edit(_ goal: Goal) {
        let vc = UIStoryboard(name: "AddOrEditGoal", bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: AddOrEditGoalVC.self)) as! AddOrEditGoalVC
        vc.coordinator = self
        vc.goalToEdit = goal
        splitVC.present(vc, animated: true, completion: nil)
    }
    
    func showDays(for goal: Goal) {
        (detailsNC.topViewController as! GoalDaysVC).goal = goal
        splitVC.showDetailViewController(detailsNC, sender: nil)
    }
    
    func makePreviewingVC(for goalDay: GoalDay) -> UIViewController {
        let vc = UIStoryboard(name: "GoalDays", bundle: Bundle.main).instantiateViewController(withIdentifier: "DayNoteVC") as! DayNoteVC
        vc.goalDay = goalDay
        return vc
    }
    
    
}

extension AppCoordinator: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
    
    
}
