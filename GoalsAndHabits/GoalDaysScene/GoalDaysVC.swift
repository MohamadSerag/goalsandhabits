import UIKit
import CoreData

class GoalDaysVC: CollectionVC<GoalDay, GoalDaysMC, DayCell>, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIViewControllerPreviewingDelegate {
    var goal: Goal! { didSet {didSetGoal()} }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cv.delegate = self
        registerForPreviewing(with: self, sourceView: cv)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationItem.backBarButtonItem?.tintColor = .red
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        navigationItem.leftBarButtonItem?.tintColor = .red
        navigationItem.leftItemsSupplementBackButton = true
    }
    
    override func initMC() -> GoalDaysMC {
        return GoalDaysMC(pc: Storage.shared.pc, cellID: "GoalDayCell", entName: "GoalDay", sortKeys: ["date"], asc: true, secKey: "month", cacheName: nil, predKey: "goal", predObj: goal, frcClass: GoalDaysMC.FormatingSecHeaderFRC.self, frcDelegate: self)
    }
    
    func didSetGoal() {
        navigationItem.title = goal.name
        if let mc = mc {
            mc.fetchDays(for: goal)
            cv.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        mc.incrementResultForDay(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/7 - 8), height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = cv.indexPathForItem(at: location) else { return nil }
        let cell = cv.cellForItem(at: indexPath) as! DayCell
        previewingContext.sourceRect = cell.frame
        return coordinator.makePreviewingVC(for: cell.goalDay)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: nil)
    }
    
}


