import CoreData
import UIKit


//MARK:- table cells

class MSTableViewCell<EntityType: Displayable>: UITableViewCell {
    var displayedObj: EntityType!
    
    func setup(with obj: EntityType) {
        
    }
}

class MSCollectionViewCell<EntityType: Displayable>: UICollectionViewCell {
    var displayedObj: EntityType!
    
    func setup(with obj: EntityType) {
        
    }
    
    func reset() {
        
    }
}


class PopupView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }


}

class MSButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 5
    }


}

class MSSplitVC: UISplitViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredDisplayMode = .allVisible
    }
}
