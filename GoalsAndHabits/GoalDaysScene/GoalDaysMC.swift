import CoreData
import UIKit


class GoalDaysMC: FrcCVDataSourceMC<GoalDay> {
    class FormatingSecHeaderFRC: NSFetchedResultsController<GoalDay> {
        override func sectionIndexTitle(forSectionName sectionName: String) -> String? {
            return Calendar.current.formatedMonthID(from: sectionName)
        }
    }
    
    private lazy var dayResults: [DayResult] = try! pc.viewContext.fetch(DayResult.fetchRequest()).sorted { a, b in a.displayOrder < b.displayOrder }
    
    override func predicateFrom(key: String, and obj: Any) -> NSPredicate? {
        return NSPredicate(format: "\(key) = %@", obj as! Goal)
    }
    
    func fetchDays(for goal: Goal) {
        frc.fetchRequest.predicate = NSPredicate(format: "goal = %@", goal)
        try! frc.performFetch()
    }
    
    func incrementResultForDay(at indexPath: IndexPath) {
        let day = frc.object(at: indexPath)
        let newIndex = dayResults.index(after: dayResults.firstIndex(of: day.result!)!)
        if newIndex == dayResults.count {
            day.result = dayResults.first!
        } else {
            day.result = dayResults[newIndex]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sview = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "GoalProgressSectionHeader", for: indexPath) as! GoalDaysSectionHeader
        let seciontInfo = frc.sections![indexPath.section]
        sview.monthLbl.text = frc.sectionIndexTitle(forSectionName: seciontInfo.name)
        return sview
        
    }


}

