import UIKit

class DayCell: MSCollectionViewCell<GoalDay> {
    static let colorProfiles = [
        "default": ["contentView": .clear, "noteIndicator": .clear, "dayLbl": #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)],
        "today": ["contentView": #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), "dayLbl": #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)],
        "lastDay": ["contentView": .yellow, "dayLbl": .red],
        "success": ["contentView": .green],
        "failure": ["contentView": .red, "dayLbl": #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)]
    ]
    @IBOutlet var noteIndicator: UIView!
    @IBOutlet var dayDateLbl: UILabel!
    @IBOutlet var dayNameLbl: UILabel!
    var goalDay: GoalDay!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 6
        contentView.layer.masksToBounds = true
    }
    
    override func setup(with obj: GoalDay) {
        goalDay = obj
        if goalDay.note != nil, !goalDay.note!.isEmpty {
            noteIndicator.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        }
        let dateComps = Calendar.current.dateComponents([.day, .month, .year], from: goalDay.date!)
        dayDateLbl.text = String(dateComps.day!)
        dayNameLbl.text = goalDay.date!.shortDayName
        if Calendar.current.isDateInToday(goalDay.date!) {
            applyColorProfile(named: "today")
        }
        if let resultName = goalDay.result?.name, resultName != "pass" {
            applyColorProfile(named: goalDay.result!.name!)
        }
    }
    
    func applyColorProfile(named name: String) {
        let profile = DayCell.colorProfiles[name]!
        contentView.backgroundColor = profile["contentView"]
        if let color = profile["dayLbl"] {
            dayDateLbl.textColor = color
            dayNameLbl.textColor = color
        }
        if let color = profile["noteIndicator"] {
            noteIndicator.backgroundColor = color
        }
    }
    
    override func reset() {
        applyColorProfile(named: "default")
        dayNameLbl.text = nil
    }
    
}

class GoalDaysSectionHeader: UICollectionReusableView {
    @IBOutlet var monthLbl: UILabel!
}


