import XCTest
@testable import GoalsAndHabits
import CoreData


class GoalsAndHabitsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let expectation = self.expectation(description: "InitialDataLoaded")
        let mpc = NSPersistentContainer(name: "GoalsAndHabits", managedObjectModel: NSManagedObjectModel.mergedModel(from: [Bundle.main])!)
        let desc = NSPersistentStoreDescription()
        desc.type = NSInMemoryStoreType
        desc.shouldAddStoreAsynchronously = false
        mpc.persistentStoreDescriptions = [desc]
        mpc.loadPersistentStores{ _, _ in }
        Storage.shared.pc = mpc
        Storage.shared.pc.mainMOC.automaticallyMergesChangesFromParent = true
        Storage.shared.loadInitialData { expectation.fulfill() }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testInitialDataLoaded() {
        let rq: NSFetchRequest<Importance> = Importance.fetchRequest()
        XCTAssertTrue(try! Storage.shared.pc.mainMOC.count(for: rq) > 0)
    }
    
    func testCreateGoal() {
        let sdate = Date()
        let edate = Calendar.current.date(byAdding: .month, value: 1, to: sdate)
        let ginfo = GoalInfo(name: "Test Goal 1", typeIndex: 0, sDate: sdate, eDate: edate, impIndex: 0)
        let addOrEditMC = AddOrEditGoalMC(pc: Storage.shared.pc)
        let g = addOrEditMC.createGoal(withInfo: ginfo)
        XCTAssertTrue(g.days!.count > 29)
        
//        if let newGoal: Goal = try! Storage.shared.pc.mainMOC.fetch(Goal.fetchRequest()).first {
//            XCTAssertTrue(newGoal.days!.count > 29)
//        } else {
//            XCTFail("Goal wasn't in the db")
//        }
    }
    
}
