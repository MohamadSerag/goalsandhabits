import CoreData
import UIKit


class AddOrEditGoalMC: MSModelCtrl {
    var goalTypes = [GoalType]()
    var goalImps = [Importance]()
    lazy var pvDataMap: [Int: Int] = [0: goalTypes.count, 1: goalImps.count, 2: GoalVisibility.allCases.count]
    
    override init(pc: NSPersistentContainer) {
        super.init(pc: pc)
        let gtReq: NSFetchRequest<GoalType> = NSFetchRequest(entityName: "GoalType")
        gtReq.sortDescriptors = [NSSortDescriptor(key: "displayOrder", ascending: true)]
        goalTypes = try! self.pc.viewContext.fetch(gtReq)
        
        let giReq: NSFetchRequest<Importance> = NSFetchRequest(entityName: "Importance")
        giReq.sortDescriptors = [NSSortDescriptor(key: "displayOrder", ascending: true)]
        goalImps = try! self.pc.viewContext.fetch(giReq)
    }
    
    private func _initDaysFor(_ goal: Goal) {
        for (displayOrder, dayDate) in Calendar.current.dayDates(from: goal.sDate!, to: goal.eDate!).enumerated() {
            if (goal.days!.contains { dayMO -> Bool in Calendar.current.isDate((dayMO as! GoalDay).date!, inSameDayAs: dayDate) }) {
                continue
            }
            let goalDay = GoalDay(context: pc.viewContext)
            goalDay.displayOrder = Double(displayOrder)
            goalDay.month = Calendar.current.monthID(from: dayDate)
            goalDay.goal = goal
            goalDay.date = dayDate
            goalDay.result = try! pc.viewContext.fetch(DayResult.fetchRequest()).first { result in result.name == "pass"}
            goalDay.month = Calendar.current.monthID(from: dayDate)
        }
    }
    
    func update(_ goal: Goal, with info: GoalInfo) {
        goal.name = info.name
        goal.type = goalTypes[info.typeIndex]
        goal.sDate = info.sDate
        goal.eDate = info.eDate
        goal.importance = goalImps[info.impIndex]
        goal.isHidden = info.visIndex == 0 ? false : true
        _initDaysFor(goal)
        pc.saveChanges() // to fix api misuse error
    }
    
    func createGoal(withInfo info: GoalInfo, at indexPath: IndexPath? = nil) {
        let goal = Goal(context: pc.viewContext)
        goal.creationDate = Date()
        goal.displayOrder = Double(Storage.shared.firstAvailableGoalDisplayOrder)
        update(goal, with: info)
    }
    
    
}

extension AddOrEditGoalMC: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pvDataMap[pickerView.tag]!
    }
    
    
}

struct GoalInfo {
    var name: String!
    var typeIndex: Int!
    var sDate: Date!
    var eDate: Date!
    var impIndex: Int!
    var visIndex: Int!
    
    
}
