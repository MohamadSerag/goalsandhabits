import UIKit


class GoalsVC: TableVC<Goal, GoalsMC, GoalCell>, UITableViewDelegate {
    @IBOutlet var editBarBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.delegate = self
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = .red
    }
    
    override func initMC() -> GoalsMC {
        return GoalsMC(pc: Storage.shared.pc, cellID: "GoalCell", entName: "Goal", sortKeys: ["displayOrder"], asc: true, secKey: nil, cacheName: nil, predKey: "isHidden = false", predObj: nil, frcDelegate: self)
    }
    
    @IBAction func addBarBtnTapped(_ sender: UIBarButtonItem) {
        coordinator.addNewGoal()
    }
    
    @IBAction func editBarBtnTapped(_ sender: UIBarButtonItem) {
        if tblView.isEditing {
            editBarBtn.title = "Edit"
            tblView.setEditing(false, animated: true)
            mc.updateDisplayOrder(of: cellDisplayedObjs)
        } else {
            editBarBtn.title = "Done"
            tblView.setEditing(true, animated: true)
        }
    }
    
    @IBAction func refreshBtnTapped() {
        mc.fetchAllGoals()
        tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblView.deselectRow(at: indexPath, animated: true)
        if tblView.isEditing {
            coordinator.edit(mc.object(at: indexPath))
        }
        coordinator.showDays(for: mc.object(at: indexPath))
    }
    
    
}


