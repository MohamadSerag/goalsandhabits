import CoreData


class Storage {
    static private let pcName = "GoalsAndHabits"
    static let shared = Storage()
    var firstAvailableGoalDisplayOrder: Int {
        return try! pc.viewContext.count(for: Goal.fetchRequest())
    }
    lazy var pc: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "\(Storage.pcName)")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in })
        return container
    }()
    var dbIsInitialized: Bool {
        let fileNames = try! FileManager.default.contentsOfDirectory(atPath: NSPersistentContainer.defaultDirectoryURL().path)
        return fileNames.contains("\(Storage.pcName).sqlite")
    }
    
    private init() {}
    
    func loadInitialData(completionHandler: (()->Void)?) {
        Storage.shared.pc.performBackgroundTask { moc in
            var types = [GoalType]()
            for (index, name) in ["Achieve Goal", "Break Habit", "Monitor Activity"].enumerated() {
                let entry = GoalType(context: moc)
                entry.name = name
                entry.displayOrder = Double(index)
                types.append(entry)
            }
            let impLevels = [
                ["0", "Normal", "default"],
                ["1", "High", "blue"],
                ["2", "Super", "red"]
            ]
            var imps = [Importance]()
            for level in impLevels {
                let imp = Importance(context: moc)
                imp.displayOrder = Double(level[0])!
                imp.name = level[1]
                imp.colorName = level[2]
                imps.append(imp)
                
            }
            let dayresults: [[String]] = [
                ["pass", "0"],
                ["success", "1"],
                ["failure", "2"]
            ]
            for elem in dayresults {
                let r = DayResult(context: moc)
                r.name = elem[0]
                r.displayOrder = Double(elem[1])!
                
            }
            
            //            let goal = Goal(context: moc)
            //            goal.creationDate = Date()
            //            goal.displayOrder = Double(0)
            //            goal.name = "Test Goal"
            //            goal.sDate = Date()
            //            goal.eDate = Calendar.current.date(byAdding: .month, value: 1, to: Date())
            //            goal.type = types.first!
            //            goal.importance = imps.first!
            try! moc.save()
            completionHandler?()
        }
    }
    
    
}

extension NSPersistentContainer {
    
    func saveChanges(in ctx: NSManagedObjectContext = Storage.shared.pc.viewContext) {
        if ctx.hasChanges {
            try! ctx.save()
        }
    }
    
    
}


