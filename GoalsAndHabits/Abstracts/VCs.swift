import UIKit
import CoreData


class MSViewCtrl<EntityType:Displayable, MCType:MSModelCtrl>: UIViewController {
    var mc: MCType!
    var coordinator: AppCoordinator!

    override func viewDidLoad() {
        super.viewDidLoad()
        mc = initMC()
    }
    
    func initMC() -> MCType {
        fatalError("Must impelemnt initMC!")
    }
    

}

class TableVC<EntityType:Displayable, MCType:FrcTVDataSourceMC<EntityType>, CellType: MSTableViewCell<EntityType>>: MSViewCtrl<EntityType, MCType>, NSFetchedResultsControllerDelegate {

    var tblView: UITableView!
    var cellDisplayedObjs: [EntityType] {
        return tblView.visibleCells.map { ($0 as! MSTableViewCell<EntityType>).displayedObj }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let tv = view.subviews[0] as? UITableView {
            tblView = tv
            tblView.dataSource = mc
        }
    }
    
    //MARK: FRC delegate methods
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tblView?.insertRows(at: [newIndexPath!], with: .automatic)
        
        case .update:
            tblView?.reloadRows(at: [indexPath!], with: .none)
        
        case .delete:
            tblView.deleteRows(at: [indexPath!], with: .automatic)
            mc.updateDisplayOrder(of: cellDisplayedObjs)
            
        case .move:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblView?.endUpdates()
    }
    
    
}

class CollectionVC<EntityType:Displayable, MCType:FrcCVDataSourceMC<EntityType>, CellType: MSCollectionViewCell<EntityType>>: MSViewCtrl<EntityType, MCType>, NSFetchedResultsControllerDelegate  {
    var cv: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let collV = view.subviews[0] as? UICollectionView {
            cv = collV
            cv.dataSource = mc
        }
    }

    //MARK: FRC delegate methods
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type == .update {
            cv.reloadItems(at: [indexPath!])
        }
    }
   
    
}



