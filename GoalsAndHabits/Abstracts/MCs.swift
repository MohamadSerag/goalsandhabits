import UIKit
import CoreData


class MSModelCtrl: NSObject {
     let pc: NSPersistentContainer
    
    init(pc: NSPersistentContainer) {
        self.pc = pc
    }
}

class FrcMC<Entity: Displayable>: MSModelCtrl {
    var frc: NSFetchedResultsController<Entity>!
    var cellID: String!
    
    init(pc: NSPersistentContainer,
         cellID: String,
         
         entName: String,
         sortKeys: [String],
         asc: Bool,
         secKey: String?,
         cacheName: String?,
         predKey: String?,
         predObj: Any?,
         frcClass: NSFetchedResultsController<Entity>.Type = NSFetchedResultsController<Entity>.self,
         frcDelegate: NSFetchedResultsControllerDelegate?,
         performFetch: Bool = true) {
        
        super.init(pc: pc)
        self.cellID = cellID
        
        // initializing FRC
        let req: NSFetchRequest<Entity> = NSFetchRequest(entityName: entName)
        req.sortDescriptors = [NSSortDescriptor]()
        for key in sortKeys {
            req.sortDescriptors!.append(NSSortDescriptor(key: key, ascending: asc))
        }
        if let predKey = predKey, let predObj = predObj {
            req.predicate = predicateFrom(key: predKey, and: predObj)!
        
        } else if let predStr = predKey {
            req.predicate = NSPredicate(format: predStr, argumentArray: nil)
        }
        frc = frcClass.init(fetchRequest: req, managedObjectContext: pc.viewContext, sectionNameKeyPath: secKey, cacheName: cacheName)
        frc.delegate = frcDelegate
        if performFetch {
            try! frc.performFetch()
        }
        // end
    }
    
    // cutomization points
    func predicateFrom(key: String, and obj: Any) -> NSPredicate? {
        return nil
    }
    
    func object(at indexPath: IndexPath) -> Entity {
        return frc.object(at: indexPath)
    }
    
    
}

class FrcTVDataSourceMC<Entity: Displayable>: FrcMC<Entity>, UITableViewDataSource {
    
    func updateDisplayOrder(of objs: [Entity]) {
        for (newIndex, obj) in objs.enumerated() {
            obj.displayOrder = Double(newIndex)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return frc.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frc.sections?[section].numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! MSTableViewCell<Entity>
        cell.setup(with: frc.object(at: indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            pc.viewContext.delete(frc.object(at: indexPath))
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

    }


}

class FrcCVDataSourceMC<Entity: Displayable>: FrcMC<Entity>, UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return frc.sections?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return frc.sections![section].numberOfObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MSCollectionViewCell<Entity>
        cell.reset()
        cell.setup(with: frc.object(at: indexPath))
        return cell
    }
    
    func sectionIndexTitle(forSectionName sectionName: String) -> String {
        return frc.sectionIndexTitle(forSectionName: sectionName)!
    }
    
    
}
