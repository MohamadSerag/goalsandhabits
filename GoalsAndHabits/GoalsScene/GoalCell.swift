import UIKit

class GoalCell: MSTableViewCell<Goal> {
    @IBOutlet var goalNameLbl: UILabel!
    @IBOutlet var subtitleLbl: UILabel!
    @IBOutlet var goalStartDateTextField: UITextField!
    @IBOutlet var goalEndDateTextField: UITextField!
    @IBOutlet var sideBarView: UIView!
    var goal: Goal!
    private var df: DateFormatter = {
        let f = DateFormatter()
        f.dateStyle = .medium
        f.timeStyle = .none
        return f
    }()
    private let colorProfiles = [
        "inActive": ["bgColor": #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)]
    ]
    
    override func setup(with obj: Goal) {
        goal = obj
        displayedObj = obj
        goalNameLbl.text = goal.name
        subtitleLbl.text = "\(goal.type!.name!) | \(goal.importance!.name!)"
        goalStartDateTextField.text = df.string(from: goal.sDate!)
        goalEndDateTextField.text = df.string(from: goal.eDate!)
        sideBarView.backgroundColor = UtilServices.colorMap[goal.type!.name!]!
    }
    
    func reset() {
        contentView.backgroundColor = .clear
    }
    
    func applyColorProfile(named name: String) {
        contentView.backgroundColor = colorProfiles[name]!["bgColor"]!
    }
    
    
}

