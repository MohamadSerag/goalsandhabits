import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if !Storage.shared.dbIsInitialized {
            Storage.shared.loadInitialData(completionHandler: nil)
        }

        appCoordinator = AppCoordinator(splitVC: MSSplitVC())
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = appCoordinator?.splitVC
        window?.makeKeyAndVisible()
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Storage.shared.pc.saveChanges()
    }
    

}


